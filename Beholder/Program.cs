﻿using System;
using libBeholder;

namespace Beholder
{
    class Program
    {
        static void Main(string[] args)
        {
            libBeholder.Beholder.NewClientEvent += (obj, arg) =>
            {
                Console.WriteLine($"YO, {((byte[])obj).UTF8()} Connected!");
            };
            libBeholder.Beholder.NewMessageEvent += (obj, arg) =>
            {
                var client = (EMessage) obj;
                Console.WriteLine($"New msg from {client.Text}");
            };
            libBeholder.Beholder.Init();
        }
    }
}
