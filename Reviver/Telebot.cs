﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using TeleBotDotNet;
using TeleBotDotNet.Requests.Methods;
using TeleBotDotNet.Responses.Types;

namespace Reviver
{
    class Telebot
    {
        private readonly TeleBot _teleBot = new TeleBot("244482223:AAHlSDLtoywHtjNM2bqBzl7kILB0gL_fFVU");
        private int _offset = 0;
        private const int MyId = 269141738;

        public IEnumerable<ControllMessage> GetMessages()
        {
            var upds = _teleBot
                .GetUpdates(new GetUpdatesRequest {Offset = _offset})
                .Result
                .Where(u => u.Message.Text.StartsWith("#!"))
                .Where(u => u.Message.Text.Split(' ').Last() == Program.FullCallName)
                .ToArray();

            if (!upds.Any()) return new ControllMessage[0];

            _offset = upds.Max(u => u.UpdateId) + 1;
            return upds.Select(u => Convert(u.Message));
        }

        private static ControllMessage Convert(MessageResponse message) => new ControllMessage(message.Text);


        public void Send(string text)
        {
            _teleBot.SendMessage(new SendMessageRequest
            {
                ChatId = MyId,
                Text = $"{Program.FullCallName} says:\n{text}"
            });
        }
        

    }
}