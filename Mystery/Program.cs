﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Albireo.Base32;
using Mono.Cecil;

namespace Mystery
{
    class Program
    {
        static void Main(string[] args)
        {
            var md = ModuleDefinition.ReadModule("DarkEyeTelegram2.exe");

            var module = md.Assembly.Modules.First();
            var types = module.Types.Where(t=>!t.HasInterfaces);
            
            var methods = types.SelectMany(t => t.Methods);
            var properties = types.SelectMany(t => t.Properties);
            
            foreach (var method in methods)
            {
                if (method.IsSpecialName  | 
                    method.Name.Contains("Main")|
                    method.Name.Contains("Dispose")
                    
                    ) continue;
               
                method.Name = Hash(method.Name,14);
            }
            foreach (var property in properties)
            {
                property.Name = Hash(property.Name,12);
            }
            
            md.Write("Out.exe");
        }

        static readonly SHA256 SHA = SHA256.Create();

        static string Hash(string s,int len)
        {
            return Base32.Encode(SHA.ComputeHash(Encoding.UTF8.GetBytes(s))).Substring(0, len);

            
        }
    }
}
