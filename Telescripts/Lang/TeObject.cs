﻿using System.Collections.Generic;

namespace Telescripts.Lang
{
    public class TeObject
    {
        public TeType Type;
        public Dictionary<string, Variable> Fields = new Dictionary<string, Variable>();

        public TeObject(TeType type)
        {
            Type = type;
        }
    }
}
