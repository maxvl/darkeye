﻿using System;
using System.Linq;
using Separse;

namespace Telescripts.Lang
{
    class TList : SExprList
    {
        public string Yankee { get; set; }
        public ISExpression[] Arguments { get; set; }

        private readonly Func<TList, ISExpression> _yankeeFunction ;

        public TList(SExprList list) : base(list)
        {
            for (var i = 1; i < Count; i++)
            {
                var element = this[i];
                if (element.Type == Types.List)
                    this[i] = new TList((SExprList) element);
            }
            Arguments = this.Skip(1).ToArray();
            Yankee = this[0].Value;
            _yankeeFunction = GetYankeeDefenition(Yankee);
        }

        public void ReloadArguments() => this.Arguments = this.Skip(1).ToArray();
        

        public ISExpression Run()
            => _yankeeFunction(this);

        internal void EvaluateArguments(int i,bool throwIfNotList=false)
        {
            if ( Arguments[i].Type != Types.List)
                if(throwIfNotList)
                    throw new ArgumentException("Argument is not a list");
                else return;

            var sexpr = (TList) Arguments[i];
            
            Arguments[i] = sexpr.Run();
        }

        private static Func<TList, ISExpression> GetYankeeDefenition(string yankee)
        {
            throw new NotImplementedException();
        }

        public void EvaluateArguments()
        {
            ReloadVariables();
            for (var i = 0; i < Arguments.Length; i++)
            {
                var element = this.Arguments[i];
                if (element.Type != Types.List)
                    continue;

               EvaluateArguments(i);
            } 
            ResolveVariables();
        }

        public void ResolveVariables(int i)
        {
            throw new NotImplementedException();

        }

        public void ResolveVariables()
        {
            for (var index = 0; index < Arguments.Length; index++)
            {
                var sexpr = Arguments[index];
                if (sexpr.Type ==Types.Symbol)
                    ResolveVariables(index);
            } 
        }

        public void ReloadVariables()
        {
            if (this.Any(sexp => sexp.Type ==Types.Symbol))
                this.ReloadArguments();
        }
    }
}
