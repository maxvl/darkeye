﻿using System.Collections.Generic;
using Telescripts.Lang;

namespace Telescripts
{
    public static class Runtime
    {
        public static VariableHolder Variables = new VariableHolder();
    }

    public class VariableHolder
    {
        public Stack<Dictionary<string, TeObject>> Stack = new Stack<Dictionary<string, TeObject>>();

        public VariableHolder()
        { AddFrame(); }

        public TeObject this[string name]
        {
            get
            {
                foreach (var frame in Stack)
                    if (frame.ContainsKey(name)) return frame[name];

                throw new KeyNotFoundException("No such variable.");
            }
            set { Stack.Peek()[name] = value; }
        }

        public void AddFrame()
        {
            Stack.Push(new Dictionary<string, TeObject>());
        }


        public void RemoveFrame()
        {
            Stack.Pop();
        }

        public void Define(string name, TeObject value) => Stack.Peek().Add(name, value);
    }
}