﻿using Telescripts.Lang;

namespace Telescripts
{
    public struct Variable
    {

        public enum TeSimpleType : byte{Bool,Int,String}

        public readonly bool IsSimple;
        public TeType Type;
        public TeObject Value;
        public object SimpleValue;
        
        public Variable(TeType type, TeObject value)
        {
            Type = type;
            Value = value;
            Value = value;
            IsSimple = false;
            SimpleValue = null;
        }
        public Variable(TeType type, string value)
        {
            Type = type;
            IsSimple = true;
            SimpleValue = value;
            Value = null;
        }
        public Variable(TeType type, bool value)
        {
            Type = type;
            IsSimple = true;
            SimpleValue = value;
            Value = null;
        }
        public Variable(TeType type, int value)
        {
            Type = type;
            IsSimple = true;
            SimpleValue = value;
            Value = null;
        }


    }
}