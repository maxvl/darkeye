﻿using System.Linq;

namespace DarkEyeTelegram2
{
    public class ControllMessage
    {
        public string Text;
        
        public ControllFile File;


        public ControllMessage(string text, ControllFile file)
        {
            Text = text;
            File = file;
        }
        /*
        public ControllMessage(Message message)
        {
            TargetingString = message.Subject.Split(' ')[0];
            Text = message.Body.Text;
            File = message.Attachments.Any() ? new ControllFile(message.Attachments[0].FileData, message.Attachments[0].FileName) 
                : null;
        }*/
    }

    public class ControllFile
    {
        public byte[] Bytes;
        public string Name;

        public ControllFile(byte[] bytes, string name)
        {
            Bytes = bytes;
            Name = name;
        }
    }
}