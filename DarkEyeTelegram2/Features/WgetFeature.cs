﻿using System;
using System.IO;
using System.Linq;
using System.Net;

namespace DarkEyeTelegram2.Features
{
    class WgetFeature : IFeature
    {
        public string FeatureName => "wget";
        public FeatureUsingResult Call(string[] arguments)
        {
            if (arguments.Length < 1) return new FeatureUsingResult("usage: wget $URL [$PATHTOSAVE]");


            Uri uriResult;
            bool isValidUrl = Uri.TryCreate(arguments[1], UriKind.Absolute, out uriResult)
                && (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps);

            if(!isValidUrl) throw new ArgumentException("Argument[1] does not looks like a valid URL : "+arguments[1]);
            var fur = new FeatureUsingResult("Url checked ok");

            var destPath = arguments.ElementAtOrDefault(2) ?? Path.GetTempPath() + new Random().Next(1,9999);
            fur.AppendTextMessage("Saving to "+destPath);
            fur.AppendTextMessage($"{DateTime.Now:hh:mm:ss} Starting download" );

            using (WebClient webClient = new WebClient())
            using (var file = File.Create(destPath))
            using (var dwnStream = webClient.OpenRead(uriResult))
                dwnStream.CopyTo(file,16384);

            fur.AppendTextMessage($"{DateTime.Now:hh:mm:ss} Finished");

            return fur;

        }

        public void Init()
        {
        }
    }
}
