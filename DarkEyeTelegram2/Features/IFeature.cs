﻿namespace DarkEyeTelegram2.Features
{
    interface IFeature
    {
        string FeatureName { get; }
        FeatureUsingResult Call(string[] arguments);
        void Init();

    }
}
