﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace DarkEyeTelegram2.Features
{
    public class SafedelFeature : IFeature
    {
        public string FeatureName { get; } = "saferm";

        public FeatureUsingResult Call(string[] args)
        {
            var path = args[1];
            if (!File.Exists(path)) return new FeatureUsingResult("No such file found");
            var fast = args.ElementAtOrDefault(2) == "fast";
            var stopwatch = Stopwatch.StartNew();
            var filesize = new FileInfo(path).Length;
            using (var writer = new BufferedStream(File.Open(path, FileMode.Open, FileAccess.ReadWrite)))
            {
                for (var i = 0; i < filesize; i++)
                    writer.WriteByte(0x00);
                writer.Flush();
                if (fast)
                {
                    stopwatch.Stop();
                    File.Delete(path);
                    return new FeatureUsingResult($"Done. {stopwatch.Elapsed}; fast mode");
                }
                writer.Position= 0;
                for (var i = 0; i < filesize; i++)
                    writer.WriteByte(0xff);
                writer.Flush();
                writer.Position = 0;
                var rand = new Random();
                var randomBytes = new byte[1024*4];
                for (var i = 0; i < filesize; i += 1024*4)
                {
                    rand.NextBytes(randomBytes);
                    writer.Write(randomBytes,0 ,randomBytes.Length);
                }

                writer.Flush();
                writer.Position = 0;
                for (var i = 0; i < filesize; i++)
                    writer.WriteByte( 0x00);
                writer.Flush();
            }
            stopwatch.Stop();
            File.Delete(path);
            return new FeatureUsingResult($"Done. {stopwatch.Elapsed}");
        }

        public FeatureUsingResult Checkout()
        {
            return new FeatureUsingResult($"Usage: {FeatureName} $FILE [fast] ");
        }

        public void Init()
        {
        }
    }
}