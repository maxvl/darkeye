﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace DarkEyeTelegram2.Features
{
    public class FeatureUsingResult
    {
        public Queue<string> TextMessages { get; }= new Queue<string>(2);

        public byte[] File
        {
            get
            {
                if (_attachmentAsBytes != null)
                    return _attachmentAsBytes;
                if (_attachmentAsFilePath != null)
                {
                    var binaryReader = new BinaryReader(System.IO.File.Open(_attachmentAsFilePath,
                        FileMode.Open, FileAccess.Read));
                    return binaryReader.ReadBytes((int) binaryReader.BaseStream.Length);
                }
                return null;
            }
        }

        private string _attachmentAsFilePath;
        private byte[] _attachmentAsBytes;
        public string AttachmentName;

        public FeatureUsingResult(string message)
        {
            TextMessages.Enqueue(message);
        }
        public FeatureUsingResult(){}

        public FeatureUsingResult(byte[] bytes, string fname)
        {
            AddAttachment(bytes, fname);
        }
        

        public void AppendTextMessage(string s)
        {
            TextMessages.Enqueue($"{DateTime.Now:MM/dd hh:mm:ss} {s}");
        }

        public void AddAttachment(byte[] attachBytes,string name)
        {
#if DEBUG
            if (_attachmentAsBytes != null) Debugger.Break();
#endif
            _attachmentAsBytes = attachBytes;
            AttachmentName = name;
        }

        public void AddAttachment(string filePath,string name)
        {
#if DEBUG
            if (_attachmentAsBytes != null) Debugger.Break();
#endif
            _attachmentAsFilePath = filePath;
            AttachmentName = name;
        }

        public override string ToString()
        {
            
            return TextMessages.Any() ? ( TextMessages.Count > 1 ?  TextMessages.Aggregate((x, y) => $"{x}\n{y}") : TextMessages.Dequeue() )
                : ""; 
        }
    }
}