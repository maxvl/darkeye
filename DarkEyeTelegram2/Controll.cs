﻿using System.Collections.Generic;
using System.IO;

namespace DarkEyeTelegram2
{
    public interface IControll  
    {
        IEnumerable<ControllMessage> GetMessages();
        void Send(string text,string name, Stream resultFile,string attachmentName);
        void Send(string text,string name);
        void Send(string text, string name, byte[] resultFile, string attachmentName);

    }
}