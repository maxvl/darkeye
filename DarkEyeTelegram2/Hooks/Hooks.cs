﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace DarkEyeTelegram2.Hooks
{
    public static class Hooks 
    {
        private static readonly List<IActiveHook> ActiveHooks = new List<IActiveHook>();
        private static readonly Thread HooksThread = new Thread(CheckHooks);
        private static readonly Random Rand = new Random();
        public static IEnumerable<string> Active => ActiveHooks.Select(hook=> $"{hook.Name} #{hook.ID}");
    

        private static void CheckHooks()
        {
            while (true)
            {
                if (ActiveHooks.Any())
                {
                    for (int index = 0; index < ActiveHooks.Count; index++)
                    {
                        IActiveHook activeHook = ActiveHooks[index];
                        if (!activeHook.Check()) continue;
                        ControllObserver.Send(activeHook.Result);

                        ActiveHooks.RemoveAt(index);
                    }
                }
                Thread.Sleep(3000);
            }
        }

        public static void Set(IHook hookType, IEnumerable<string> args) 
        {

            ActiveHooks.Add(hookType.SetHook(Rand.Next(1000,9999),args));
        }

        public static void Unset(int id)
        {
            ActiveHooks.RemoveAll(hook => hook.ID == id);
        }
        public static void Init()
        {
            HooksThread.Start();
        }
    }
}