﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using DarkEyeTelegram2.Features;

namespace DarkEyeTelegram2.Hooks
{
    public class WebCamHook : IHook
    {
        public string InvocationName { get; } = "webcam";

        public IActiveHook SetHook(int id, IEnumerable<string> args)
        {
            return new WebCamActiveHook(id);
        }
    }

    public class WebCamActiveHook : IActiveHook
    {
        private readonly WebcamFeature _webcamService;
        private readonly Bitmap _comparingImg;
        private readonly long _threshold;

        public WebCamActiveHook(long threshold = 20000)
        {
            this._threshold = threshold;
            ID = new Random().Next(1000, 9999);
            _webcamService = (WebcamFeature) Program.Features.First(s => s.FeatureName == "webcam");
            if (!_webcamService.DllReady) _webcamService.CopyDll();
            _comparingImg = PreprocessImage(_webcamService.TakePic());
        }

        private Bitmap PreprocessImage(Image picture)
        {
            var th = picture.GetThumbnailImage(64, 64, () => false, IntPtr.Zero);
            var bth = new Bitmap(th);
            for (int x = 0; x < bth.Width; x++)
            {
                for (int y = 0; y < bth.Height; y++)
                {
                    var rcolor = bth.GetPixel(x, y).R;
                    var greycolor = Color.FromArgb(rcolor, rcolor, rcolor);
                    bth.SetPixel(x, y, greycolor);
                }
            }
            th.Dispose();

            return bth;
        }

        public void Dispose()
        {
            _comparingImg.Dispose();
        }

        public int ID { get; }

        public bool Check()
        {
            var newimg = PreprocessImage(_webcamService.TakePic());
            long diff = 0;
            using (var nbitmap = new Bitmap(newimg))
            {
                for (int x = 0; x < nbitmap.Width; x++)
                {
                    for (int y = 0; y < nbitmap.Height; y++)
                    {
                        diff += Math.Abs(nbitmap.GetPixel(x, y).R - _comparingImg.GetPixel(x,y).R);
                    }
                }
            }
            Debug.WriteLine(diff);
            return _threshold < diff;
        }

        public FeatureUsingResult Result { get; } = new FeatureUsingResult("Webcam image changed!");
        public string Name { get; } = "webcamhook";
    }
}