﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Windows.Forms;
using DarkEyeTelegram2.Features;

namespace DarkEyeTelegram2.Hooks
{
    public class MouseHook : IHook
    {
        public string InvocationName { get; } = "mouse";
        public IActiveHook SetHook(int id, IEnumerable<string> args)
        {

            return new MouseActiveHook(id);
        }

        class MouseActiveHook : IActiveHook
        {
            private readonly int x;
            private readonly int y;

            public MouseActiveHook(int id)
            {
                ID = id;
                x = Cursor.Position.X;
                y = Cursor.Position.Y;
            }

            public void Dispose(){}

            public int ID { get; }

            public bool Check()
            {
                var pos = Cursor.Position;
                Debug.WriteLine($"x: {x}, y: {y}\npos.x: {pos.X} pos.x:{pos.Y}\n");

                bool b = pos.X != x && pos.Y != y;
                return b;
            }



            public FeatureUsingResult Result => new FeatureUsingResult("Mouse moved!");

            public string Name { get; }
        }
    }
}