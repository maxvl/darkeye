﻿using System.Collections.Generic;
using System.IO;
using DarkEyeTelegram2.Features;

namespace DarkEyeTelegram2.Hooks
{
    public class DiskHook : IHook
    {
        public string InvocationName => "disks";
        public IActiveHook SetHook(int id, IEnumerable<string> args)
        {
            return new ActiveDiskHook(id);
        }
    }

    public class ActiveDiskHook : IActiveHook
    {
        private readonly DriveInfo[] driveInfo;
        public ActiveDiskHook(int id)
        {
            ID = id;
            driveInfo = DriveInfo.GetDrives();

        }

        public void Dispose() { }

        public int ID { get; }
        public bool Check()
        {
            var newDriveInfo = DriveInfo.GetDrives();
            return newDriveInfo.Length != driveInfo.Length;
        }

        public FeatureUsingResult Result => new FeatureUsingResult("Drive attached or mooved!");
        public string Name { get; } = "diskhook";
    }
}