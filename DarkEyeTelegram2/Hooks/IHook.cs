﻿using System;
using System.Collections.Generic;
using DarkEyeTelegram2.Features;

namespace DarkEyeTelegram2.Hooks
{
    public interface IHook
    {
        string InvocationName { get; }
        IActiveHook SetHook(int id,IEnumerable<string> args);
    }

    public interface IActiveHook :IDisposable
    {
        int ID { get; }
        bool Check();
        FeatureUsingResult Result { get; }
        string Name { get; }
    }
}