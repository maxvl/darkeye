﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using DarkEyeTelegram2.Features;

namespace DarkEyeTelegram2.Hooks
{
    public class ProcessHook : IHook
    {
        public string InvocationName { get; } = "process";

        public IActiveHook SetHook(int id, IEnumerable<string> args)
        {
            return new ActiveProcessHook(id,args.ElementAt(2));
        }

        class ActiveProcessHook  : IActiveHook
        {
            private string processName;

            internal ActiveProcessHook(int id ,string processName)
            {
                ID = id;
                this.processName = processName;
            }

            public void Dispose()
            {
                processName = null;
            }

            public int ID { get; }

            public bool Check()
            {
                return Process.GetProcesses().Select(p => p.ProcessName).Contains(processName);
            }


            public FeatureUsingResult Result => new FeatureUsingResult($"\"{processName}\" executed!");
            public string Name => $"Process Hook  # {processName}";
        }
    }
}