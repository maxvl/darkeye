﻿using System;
using System.Threading;
using DarkEyeTelegram2.Features;

namespace DarkEyeTelegram2.Regeneration
{
    public static class Regeneration
    {
        private static readonly Thread CheckingThread = new Thread(Check);

        public static IRegenerator[] Regenerators =
        {
            new RegRegenerator()
        };

        private static void Check()
        {
            while (true)
            {
                try
                {
                    foreach (var regenerator in Regenerators)
                    {
        
                        if(Settings.Get($"regen.{regenerator.Name}.active")!="true") continue;
                        if (regenerator.Check())
                            continue;
                        FeatureUsingResult fur =
                            new FeatureUsingResult(regenerator.Name + "Detected problem, curing...");
                        try
                        {
                            regenerator.DoCure();
                        }
                        catch (Exception e)
                        {
                            fur.AppendTextMessage($"ERROR:\n{e.Message}\n{e.StackTrace}");
                            ControllObserver.Send(fur);
                            continue;
                        }
                        fur.AppendTextMessage("Cured.");
                        ControllObserver.Send(fur);
                    }
#if DEBUG
                    Thread.Sleep(5000);
#else
                    Thread.Sleep(20000);

#endif
                }
                catch (Exception e)
                {
                    ControllObserver.ReportError(e);
                }
            }
        }

        public static void Init()
        {
            CheckingThread.Start();
        }

        public static void Abort()
        {
            CheckingThread.Abort();
        }

        public static void Off(string s)
        {
            Settings.Set($"regen.{s}.active","false");
        }

        public static void On(string s)
        {
            Settings.Set($"regen.{s}.active", "true");
        }
    }
}
