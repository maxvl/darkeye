﻿namespace DarkEyeTelegram2.Regeneration
{
    public interface IRegenerator
    {
        string Name { get; }
        bool Check();
        void DoCure();

    }
}
