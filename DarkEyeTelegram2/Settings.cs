﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace DarkEyeTelegram2
{
    public static class Settings
    {
        private static readonly Dictionary<string, string> Values = new Dictionary<string, string>();
        private static readonly Dictionary<string, string> Defaults = new Dictionary<string, string>
        {
            {"regen.regregen.active","false"},
        };


        private static string _path;

        public static void Init(string path)
        {
            _path = path;
            Read();
        }


        public static void Write()
        {
            using (MemoryStream mem = new MemoryStream())
            using (BinaryWriter wrt = new BinaryWriter(File.Open(_path, FileMode.Create)))
            {
                foreach (var pair in Values)
                {
                    var b = Encoding.UTF8.GetBytes(pair.Key + "=" + pair.Value + "\n");
                    mem.Write(b, 0, b.Length);
                }
                wrt.Write(AESEncryptBytes(mem.ToArray(), "P%%AAXkl5%%%))acscs_casfsh3e3@@$225%%%%%aszd8(*2$%@sa"));
                wrt.Flush();
            }
        }

        public static void Read()
        {
            if(!File.Exists(_path))
                return;
            
            var lines = Encoding.UTF8
                .GetString(
                    AESDecryptBytes(File.ReadAllBytes(_path), "P%%AAXkl5%%%))acscs_casfsh3e3@@$225%%%%%aszd8(*2$%@sa"))
                .Split('\n');

            foreach (string line in lines)
            {
                if (string.IsNullOrEmpty(line)) continue;
                var splited = line.Split('=');
                Values[splited[0]] = splited[1];
            }

        }

        public static string Get(string name)
        {
            string value;
            var r = Values.TryGetValue(name, out value) ? value : Defaults[name];
            return r;
        }
        public static void Set(string name, string value)
        {
            Values[name] = value;
            Write();
        }

        private static byte[] AESEncryptBytes(byte[] input, string password)
        {

            using (var aes = new AesCryptoServiceProvider
            {
                KeySize = 128,
                BlockSize = 128,

                Mode = CipherMode.CBC,
                Padding = PaddingMode.PKCS7
            })
            {
                aes.GenerateIV();
                var iv = aes.IV;
                using (MD5 md = new MD5Cng())
                    aes.Key = md.ComputeHash(Encoding.UTF8.GetBytes(password));

                using (var encrypter = aes.CreateEncryptor(aes.Key, iv))
                {
                    using (var cipherStream = new MemoryStream())
                    {
                        using (var tCryptoStream = new CryptoStream(cipherStream, encrypter, CryptoStreamMode.Write))
                        {
                            using (var tBinaryWriter = new BinaryWriter(tCryptoStream))
                            {
                                //Prepend IV to data
                                //tBinaryWriter.Write(iv); This is the original broken code, it encrypts the iv
                                cipherStream.Write(iv, 0, iv.Length); //Write iv to the plain stream (not tested though)
                                tBinaryWriter.Write(input);
                                tCryptoStream.FlushFinalBlock();
                            }
                        }
                        var _ = cipherStream.ToArray();
                        return _;
                    }
                }
            }
        }
        private static byte[] AESDecryptBytes(byte[] input, string password)
        {
            using (var aes = new AesCryptoServiceProvider
            {
                KeySize = 128,
                BlockSize = 128,
                Mode = CipherMode.CBC,
                Padding = PaddingMode.PKCS7
            })
            {

                //get first 16 bytes of IV and use it to decrypt
                var iv = new byte[16];
                Array.Copy(input, 0, iv, 0, iv.Length);

                using (MD5 md = new MD5Cng())
                    aes.Key = md.ComputeHash(Encoding.UTF8.GetBytes(password));

                using (var ms = new MemoryStream())
                {
                    using (var cs = new CryptoStream(ms, aes.CreateDecryptor(aes.Key, iv), CryptoStreamMode.Write))
                    {
                        using (var binaryWriter = new BinaryWriter(cs))
                        {
                            //Decrypt Cipher Text from Message
                            binaryWriter.Write(
                                input,
                                iv.Length,
                                input.Length - iv.Length
                            );
                        }
                    }
                    var _ = ms.ToArray();
                    return _;
                }
            }
        }
    }
}