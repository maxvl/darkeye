﻿using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;

namespace DarkEyeTelegram2
{/*
    public static class EventPlanner
    {
        private static readonly string FilePath = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile)+
                                                 @"\AppData\Local\Microsoft\CoreSerts.bin";

        public static void Init()
        {
            if (!File.Exists(FilePath))
                return;
            
            

        }

        
        private static byte[] AESEncryptBytes(byte[] input, byte[] passwordBytes, bool cbc = true)
        {

            using (var aes = new AesCryptoServiceProvider
            {
                KeySize = 256,
                BlockSize = 128,
                Key = passwordBytes,
                Mode = cbc ? CipherMode.CBC : CipherMode.ECB,
                Padding = PaddingMode.PKCS7
            })
            {
                aes.GenerateIV();
                var iv = aes.IV;
                using (SHA256 sha = new SHA256Managed())
                    aes.Key = sha.ComputeHash(passwordBytes);

                using (var encrypter = aes.CreateEncryptor(aes.Key, iv))
                {
                    using (var cipherStream = new MemoryStream())
                    {
                        using (var tCryptoStream = new CryptoStream(cipherStream, encrypter, CryptoStreamMode.Write))
                        {
                            using (var tBinaryWriter = new BinaryWriter(tCryptoStream))
                            {
                                //Prepend IV to data
                                //tBinaryWriter.Write(iv); This is the original broken code, it encrypts the iv
                                cipherStream.Write(iv, 0, iv.Length); //Write iv to the plain stream (not tested though)
                                tBinaryWriter.Write(input);
                                tCryptoStream.FlushFinalBlock();
                            }
                        }
                        var _ = cipherStream.ToArray();
                        return _;
                    }
                }
            }
        }
        private static byte[] AESDecryptBytes(byte[] input, byte[] passwordBytes, bool cbc = true)
        {
            using (var aes = new AesCryptoServiceProvider
            {
                KeySize = 256,
                BlockSize = 128,
                Mode = cbc ? CipherMode.CBC : CipherMode.ECB,
                Padding = PaddingMode.PKCS7
            })
            {

                //get first 16 bytes of IV and use it to decrypt
                var iv = new byte[16];
                Array.Copy(input, 0, iv, 0, iv.Length);

                using (SHA256 sha = new SHA256Managed())
                    aes.Key = sha.ComputeHash(passwordBytes);

                using (var ms = new MemoryStream())
                {
                    using (var cs = new CryptoStream(ms, aes.CreateDecryptor(aes.Key, iv), CryptoStreamMode.Write))
                    {
                        using (var binaryWriter = new BinaryWriter(cs))
                        {
                            //Decrypt Cipher Text from Message
                            binaryWriter.Write(
                                input,
                                iv.Length,
                                input.Length - iv.Length
                            );
                        }
                    }
                    var _ = ms.ToArray();
                    return _;
                }
            }
        }
    }*/
}