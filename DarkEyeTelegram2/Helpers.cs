﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace DarkEyeTelegram2
{
    static class Helpers
    {
        private static readonly char[] EngChars = "qwertyuiop[]asdfghjkl;'zxcvbnm,./`".ToCharArray();
        private static readonly char[] RusChars = "йцукенгшщзхъфывапролджэячсмитьбю.ё".ToCharArray();

        public static void ForEach<T>(this IEnumerable<T> enumerable, Action<T> action)
        {
            foreach (var o in enumerable)
                action(o);
        }
        


        public static byte[] ImageToBytes(this Image img)
        {
            byte[] byteArray;
            using (var stream = new MemoryStream())
            {
                img.Save(stream, ImageFormat.Png);
                stream.Close();

                byteArray = stream.ToArray();
            }
            return byteArray;
        }

        public static string Layout(this string s, bool isRussian)
        {
            try
            {
                if (isRussian && (s.Length == 1))
                    return RusChars[Array.IndexOf(EngChars, s[0])].ToString();
                return s;
            }
            catch (Exception)
            {
                return "_?_";
            }
        }
    }
}
