﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace DarkEyeTelegram2
{
    public static class Tasks
    {
        private static readonly Dictionary<int, ScheduledTask> ActiveTasks = new Dictionary<int, ScheduledTask>();

        public static string TasksIds => string.Join(", ", ActiveTasks.Keys);
        internal static string Add(ScheduledTask scheduledTask)
        {
            var random = new Random();
            int key;
            int i = 0;
            gen_key:
                i++;
                if (i == 10) throw new Exception("Cant generate task key");
                key = random.Next(1000, 9999);
                if (ActiveTasks.ContainsKey(key))
                    goto gen_key;
            ActiveTasks.Add(key, scheduledTask);

            return key.ToString();
        }

        public static void Kill(int taskId)
        {
            ActiveTasks[taskId].Kill();
            ActiveTasks.Remove(taskId);
        }

        public static void KillAll(out int total)
        {
            int i = 0;
            var ids = ActiveTasks.Keys.ToArray();
            foreach (var task in ids)
            {
                Kill(task);
                i++;
            }
            total = i;
        }
    }

    internal class ScheduledTask
    {
        public int Sleeping { get; set; }
        public string Command { get; set; }
        public Timer TaskTimer;
        private void DoTask()
        {
            ControllObserver.ExecAndSend(Command);
        }

        internal ScheduledTask(string timestr, string cmd)
        {
            Command = cmd;
            int sleep = 10000;
            switch (timestr.Last())
            {
                case 's':
                    sleep = (int)Convert.ToSingle(timestr.Substring(0, timestr.Length - 1)) * 1000;
                    break;
                case 'm':
                    sleep = (int)(Convert.ToSingle(timestr.Substring(0, timestr.Length - 1)) * 1000 * 60);
                    break;
                case 'h':
                    sleep = (int)(Convert.ToSingle(timestr.Substring(0, timestr.Length - 1)) * 1000 * 60 * 60);
                    break;
                default:
                    int val;
                    if (int.TryParse(timestr, out val))
                        sleep = val;
                    break;
            }
            Sleeping = sleep;
            TaskTimer = new Timer(state => DoTask(), null, Sleeping, Sleeping);
        }


        public void Kill()
        {
            this.TaskTimer.Dispose();
            this.TaskTimer = null;
        }
    }
}