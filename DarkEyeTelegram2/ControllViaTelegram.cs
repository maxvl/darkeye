﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using TeleBotDotNet;
using TeleBotDotNet.Extensions;
using TeleBotDotNet.Requests.Methods;
using TeleBotDotNet.Requests.Types;
using TeleBotDotNet.Responses.Types;

namespace DarkEyeTelegram2
{
    public class ControllViaTelegram : IControll
    {
        private readonly TeleBot _teleBot = new TeleBot(PData.A);
        private int _offset = 0;
        




        public IEnumerable<ControllMessage> GetMessages()
        {
            var upds = _teleBot
                .GetUpdates(new GetUpdatesRequest {Offset = _offset})
                .Result
                .Where(CheckTargeting)
                .ToArray();

            if (!upds.Any()) return new ControllMessage[0];

            _offset = upds.Max(u => u.UpdateId) + 1;
            return upds.Select(u => Convert(u.Message));
        }

        private bool CheckTargeting(UpdateResponse upd)
        {
            if (upd.Message?.Text == null) return true;
            var last = upd.Message.Text.Split(' ').Last();
            return CheckTargeting(last) || !last.StartsWith("@");
        }

        private ControllMessage Convert(MessageResponse message)
        {
            var cm = new ControllMessage(
                message.Text ?? "",
                message.Document != null
                    ? new ControllFile(_teleBot.DownloadFile(_teleBot.GetFile(new GetFileRequest()
                    {
                        FileId = message.Document.FileId
                    }
                    )), message.Document.FileName)
                    : null
            );
            return cm;
        }

        public static bool CheckTargeting(string targetingString) 
            => targetingString == "@*" || Program.FullCallName == targetingString;

        public void Send(string text, string name, Stream resultFile, string attachmentName)
            => Send(text, name, ReadFully(resultFile), attachmentName);
        private static byte[] ReadFully(Stream input)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                input.CopyTo(ms);
                return ms.ToArray();
            }
        }
        public void Send(string text, string name)
        {
            _teleBot.SendMessage(new SendMessageRequest
            {
                ChatId = PData.I,
                Text = $"{Program.FullCallName} says:\n{text}"
            });
        }

        public void Send(string text, string name, byte[] resultFile, string attachmentName)
        {

            if (!string.IsNullOrEmpty(text))
                Send(text, name);
            _teleBot.SendDocument(new SendDocumentRequest()
            {
                ChatId = PData.I,
                Document = new InputFileRequest()
                {
                    FileExtension = attachmentName,
                    File = resultFile
                }
            });
        }
    }
}
